# Quantum Kinetic description of Higher Harmonic Generation

Theoretical Physics internship-project @ UT at Austin. Internship for Master applied physics (nano-track) @ TU/e.

Research group: Allan H Macdonald's group


## Repository structure

This repository is the root of the project.
The different parts of this project (thesis, computational part, etc.) are checked out in branches.
These different parts/branches are included as submodules in the master branch.

Literature, photos, etc. are excluded from git (using .gitignore) for copyright and privacy reasons.

